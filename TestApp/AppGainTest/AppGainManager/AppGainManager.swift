//
//  AppGainManager.swift
//  AppGainTest
//
//  Created by Monica Girgis Kamel on 12/09/2023.
//

import Foundation
import Appgain
import Appgain_rich

class AppGainManager {
    
    public static let shared = AppGainManager()
    
    private init() {}
    
    
    func updateUserId() {
        Spinner.showSpinner()
        if let id = Appgain.getUserID() {
            Spinner.hideSpinner()
            Appgain.updateUserId(id) { response, dict, error in
                print(response)
                print(dict)
                print(error)
            }
        }
    }
}
