//
//  DynamicLinkGenerator.swift
//  MoviesApp
//
//  Created by Monica Girgis Kamel on 18/08/2023.
//

import Foundation

class DynamicLinkGenerator {
    static func generateDynamicLink() -> URL? {
        let baseUrl = "TestApp://details_screen"
        return URL(string: baseUrl)
    }
}
