//
//  MySideMenuViewController.swift
//  AppGainTest
//
//  Created by Monica Girgis Kamel on 09/10/2023.
//

import UIKit
import SideMenu

class MySideMenuViewController: SideMenuController {

    override func viewDidLoad() {
        
        self.menuViewController = SideMenuItemsViewController()
        self.contentViewController = TestViewController()
        
        
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.contentViewController.view.frame = view.bounds
    }

}
