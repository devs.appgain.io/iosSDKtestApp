//
//  AboutViewController.swift
//  AppGainTest
//
//  Created by Monica Girgis Kamel on 08/10/2023.
//

import UIKit
import WebKit

class AboutViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        title = "About"
        
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        let url = URL(string: "https://www.appgain.io/about")!
        let urlRequest = URLRequest(url: url)
        
        Spinner.showSpinner()
        webView.load(urlRequest)
    }

}

extension AboutViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }
            Spinner.hideSpinner()
            heightConstraint.constant = webView.scrollView.contentSize.height
        }
    }
}
