//
//  SideMenuItemsViewController.swift
//  AppGainTest
//
//  Created by Monica Girgis Kamel on 09/10/2023.
//

import UIKit
import SideMenu

class Preferences {
    static let shared = Preferences()
    var enableTransitionAnimation = false
}

class SideMenuItemsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    
    private var testCases: [SideMenuItems] = [.PrivacyPolicy, .AboutPage, .RequestDemo]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSideMenuUI()
        setupTableViewUI()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let sideMenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sideMenuBasicConfiguration.position == .under) != (sideMenuBasicConfiguration.direction == .right)
        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
        view.layoutIfNeeded()
    }
    
    func setupTableViewUI() {
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        tableView.register(UINib(nibName: TestActionsTableViewCell.className, bundle: nil), forCellReuseIdentifier: TestActionsTableViewCell.className)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setSideMenuUI() {
        selectionMenuTrailingConstraint.constant = 0
        
        let sidemenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sidemenuBasicConfiguration.position == .under) != (sidemenuBasicConfiguration.direction == .right)
        if showPlaceTableOnLeft {
            selectionMenuTrailingConstraint.constant = SideMenuController.preferences.basic.menuWidth - view.frame.width
        }
    }

}

extension SideMenuItemsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        testCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TestActionsTableViewCell.className, for: indexPath) as! TestActionsTableViewCell
        cell.setData(title: testCases[indexPath.row].title, image: testCases[indexPath.row].image)
        cell.setCellStyle(backgoundColor: .clear, style: .gray)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sideMenuController?.hideMenu()
        
        let itemSelected = testCases[indexPath.row]
        switch itemSelected {
        case .PrivacyPolicy:
            let vc = PrivacyPolicyViewController()
            navigationController?.pushViewController(vc, animated: true)
        case .AboutPage:
            let vc = AboutViewController()
            navigationController?.pushViewController(vc, animated: true)
        case .RequestDemo:
            let vc = RequestDemoViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

