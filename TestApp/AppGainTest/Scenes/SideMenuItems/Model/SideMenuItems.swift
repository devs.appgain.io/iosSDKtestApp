//
//  SideMenuItems.swift
//  AppGainTest
//
//  Created by Monica Girgis Kamel on 08/10/2023.
//

import Foundation

enum SideMenuItems {
    case PrivacyPolicy
    case AboutPage
    case RequestDemo
    
    var title: String {
        switch self {
        case .PrivacyPolicy:
            return "Privacy Policy"
        case .AboutPage:
            return "About Page"
        case .RequestDemo:
            return "Request A Demo"
        }
    }
    
    var image: String {
        switch self {
        case .PrivacyPolicy:
            return "privacy policy"
        case .AboutPage:
            return "about"
        case .RequestDemo:
            return "requestDemo"
        }
    }
}
