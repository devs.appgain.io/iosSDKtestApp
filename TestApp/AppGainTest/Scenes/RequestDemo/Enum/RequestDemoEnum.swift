//
//  RequestDemoEnum.swift
//  AppGainTest
//
//  Created by Monica Girges on 09/10/2023.
//

import Foundation

enum RequestDemoSections {
    case basicData
    case countries
    case saveAction
}

enum RequestDemoEnum {
    case fullName
    case workEmail
    case phoneNumber
    case companyName
    case companyWebsite
    case chooseCountry
    
    var title: String {
        switch self {
        case .fullName:
            return "Full Name"
        case .workEmail:
            return "Work Email"
        case .phoneNumber:
            return "Phone Number"
        case .companyName:
            return "Company Name"
        case .companyWebsite:
            return "Company Website (Optional)"
        case .chooseCountry:
            return "Choose Country"
        }
    }
    
    var subTitle: String {
        switch self {
        case .fullName:
            return "Enter Your Full Name"
        case .workEmail:
            return "Enter Your Work Email"
        case .phoneNumber:
            return "Enter Your Phone Number"
        case .companyName:
            return "Enter Your Company Name"
        case .companyWebsite:
            return "Enter Your Company Website"
        case .chooseCountry:
            return "Choose Your Country"
        }
    }
}
