//
//  RequestDemoViewController.swift
//  AppGainTest
//
//  Created by Monica Girges on 09/10/2023.
//

import UIKit
import Appgain

class RequestDemoViewController: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    
    private var requestCases: [RequestDemoEnum] = [.fullName, .workEmail , .phoneNumber, .companyName , .companyWebsite, .chooseCountry]
    private var countries: [String] = ["Egypt", "Alegria", "Andorra", "France"]
    private var sections: [RequestDemoSections] = [.basicData, .saveAction]
    
    private var selectedCountry: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableViewUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.prefersLargeTitles = false
    }

    func setupTableViewUI() {
        title = "Request A Demo"
        tableView.register(UINib(nibName: RequestDemoTableViewCell.className, bundle: nil), forCellReuseIdentifier: RequestDemoTableViewCell.className)
        tableView.register(UINib(nibName: SendTableViewCell.className, bundle: nil), forCellReuseIdentifier: SendTableViewCell.className)
        tableView.register(UINib(nibName: CountryTableViewCell.className, bundle: nil), forCellReuseIdentifier: CountryTableViewCell.className)
        tableView.delegate = self
        tableView.dataSource = self
        mainView.layer.cornerRadius = 8
        mainView.layer.masksToBounds = true
        tableHeight.constant = CGFloat((requestCases.count * 100) + 90)
        self.tableView.isScrollEnabled = false
        self.scrollview.bounces = false
        self.tableView.bounces = true
    }
    
    private func handleCountriesSection() {
        if sections.contains(.countries) {
            sections.remove(at: 1)
            tableHeight.constant = CGFloat((requestCases.count * 100) + 90)
        }else{
            sections.insert(.countries, at: 1)
            tableHeight.constant = CGFloat((requestCases.count * 100) + 90 + (countries.count * 56))
        }
        
    }
}

extension RequestDemoViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .basicData:
            return requestCases.count
        case .countries:
            return countries.count
        case .saveAction:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .basicData:
            let cell = tableView.dequeueReusableCell(withIdentifier: RequestDemoTableViewCell.className, for: indexPath) as! RequestDemoTableViewCell
            cell.titleLabel.text = requestCases[indexPath.row].title
            cell.answerTextField.placeholder = requestCases[indexPath.row].subTitle
            
            if requestCases[indexPath.row] == .chooseCountry {
                cell.answerTextField.text = selectedCountry
                cell.answerTextField.isUserInteractionEnabled = false
            }else{
                cell.answerTextField.text = ""
                cell.answerTextField.isUserInteractionEnabled = true
            }
            
            return cell
            
        case .saveAction:
            let cell = tableView.dequeueReusableCell(withIdentifier: SendTableViewCell.className, for: indexPath) as! SendTableViewCell
            return cell
            
        case .countries:
            let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.className, for: indexPath) as! CountryTableViewCell
            cell.setData(title: countries[indexPath.row], image: "")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .basicData:
            let itemSelected = requestCases[indexPath.row]
            switch itemSelected {
            case .chooseCountry:
                handleCountriesSection()
                
            default:
                return
            }
            
        case .countries:
            selectedCountry = countries[indexPath.row]
            handleCountriesSection()
            
            
        default:
            return
        }
        
        tableView.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollview {
            tableView.isScrollEnabled = (self.scrollview.contentOffset.y >= 200)
        }

        if scrollView == self.tableView {
            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)
        }
    }
}
