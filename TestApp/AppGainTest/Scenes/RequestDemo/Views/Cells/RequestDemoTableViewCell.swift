//
//  RequestDemoTableViewCell.swift
//  AppGainTest
//
//  Created by Monica Girges on 09/10/2023.
//

import UIKit

class RequestDemoTableViewCell: UITableViewCell {

    @IBOutlet weak var answerTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        answerTextField.makeShadowsWith(color: UIColor.darkGray.cgColor, opacity: 0.2, shadowRadius: 1.0)
    }
    
}
