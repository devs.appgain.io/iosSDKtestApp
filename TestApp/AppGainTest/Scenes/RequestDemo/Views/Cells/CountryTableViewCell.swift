//
//  CountryTableViewCell.swift
//  AppGainTest
//
//  Created by Monica Girges on 09/10/2023.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        mainView.makeBorders(borderColor: .systemGray5)
    }

    func setData(title: String, image: String) {
        countryName.text = title
    }
}
