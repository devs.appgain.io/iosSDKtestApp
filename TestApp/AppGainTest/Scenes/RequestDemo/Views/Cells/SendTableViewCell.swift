//
//  SendTableViewCell.swift
//  AppGainTest
//
//  Created by Monica Girges on 09/10/2023.
//

import UIKit

class SendTableViewCell: UITableViewCell {

    @IBOutlet weak var sendButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        sendButton.layer.cornerRadius = 8
        sendButton.layer.masksToBounds = true
    }
    
}
