//
//  TestViewController.swift
//  AppGainTest
//
//  Created by Monica Girges on 07/09/2023.
//

import UIKit
import Appgain

class TestViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    private var testCases: [AppGainTestEnum] = [.initalize, .matchLink, .fireAutoMator, .tracking, .fireAutoMatorWith , .addPurchase, .addEmailChannel , .addSmsCahnnel, .logEvent , .toggleNotification, .getUserID, .updateUser, .updateUserID]
    private static var appID = ""
    private static var apiKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableViewUI()
        setupMenuButton()
    }

    private func setupTableViewUI() {
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        tableView.makeRoundedCornersWith(radius: 16.0)
        tableView.register(UINib(nibName: TestActionsTableViewCell.className, bundle: nil), forCellReuseIdentifier: TestActionsTableViewCell.className)
        tableView.delegate = self
        tableView.dataSource = self
        heightConstraint.constant = CGFloat((testCases.count * 75) + 16)
        scrollView.bounces = false
        tableView.bounces = true
        scrollView.layoutIfNeeded()
        scrollView.layoutSubviews()
        view.layoutIfNeeded()
        view.layoutSubviews()
    }
    
    private func setupMenuButton() {
        menuButton.makeRoundedCornersWith(radius: 8.0)
        menuButton.setTitle("", for: .normal)
    }
    
    // MARK: - initialize SDK
    private func initializeSDK() {
        if TestViewController.appID.isEmpty {
            let alertController = UIAlertController(title: "Enter Data", message: nil, preferredStyle: .alert)
            alertController.addTextField { textField in
                textField.placeholder = "App ID"
            }
            alertController.addTextField { textField in
                textField.placeholder = "API Key"
            }
            alertController.addTextField { textField in
                textField.placeholder = "SubDomain"
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                // Handle the OK button tap
                if let textFields = alertController.textFields {
                    if textFields.count >= 2 {
                        if let appID = textFields[0].text, let apiKey = textFields[1].text, let subdomain = textFields[2].text,
                           !appID.isEmpty && !apiKey.isEmpty, !subdomain.isEmpty {
                            TestViewController.appID = appID
                            TestViewController.apiKey = apiKey
                            Spinner.showSpinner()
                            Appgain.initialize(appID, apiKey: apiKey, subDomain: subdomain, trackUserForAdvertising: true) { [weak self] response, dict, error in
                                guard let self = self else { return }
                                Spinner.hideSpinner()
                                print(response)
                                print(dict)
                                print(error)
                                if let httpResponse = response as? HTTPURLResponse {
                                    if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                        self.showMessage(title: "Success", message: "Success")
                                    } else {
                                        self.showMessage(title: "Error", message: response?.description ?? "")
                                    }
                                }else {
                                    self.showMessage(title: nil, message: (dict?["success"] as? String) ?? "")
                                }
                            }
                        } else {
                            self.showMessage(title: "Error", message: "Please Enter appID & apiKey")
                        }
                    }
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        } else {
            self.showMessage(title: "Error", message: "Already Initalize")
        }
    }
    
    // MARK: - match link
    private func matchLink() {
        Spinner.showSpinner()
        Appgain.matchLink { response, data, error in
            Spinner.hideSpinner()
            print("MatchLink is:", response, data, error)
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                    self.showMessage(title: "Success", message: "Success")
                } else {
                    self.showMessage(title: "Error", message: response?.description ?? "")
                }
            }else {
                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    // MARK: - fireAutoMator
    private func fireAutoMator() {
        let alertController = UIAlertController(title: "Enter Trigger Point", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Trigger Point"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 1 {
                    if let triggerPoint = textFields[0].text, !triggerPoint.isEmpty {
                        Spinner.showSpinner()
                        Appgain.fireAutomator(triggerPoint, personalizationData: nil) {[weak self] response, dic, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                            print("fireAutoMator is:", response, dic, error)
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter trigger point")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func fireAutomatorWithPersonalization() {
        let alertController = UIAlertController(title: "Enter Data", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Trigger point name"
        }
        alertController.addTextField { textField in
            textField.placeholder = "name"
        }
        alertController.addTextField { textField in
            textField.placeholder = "value"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 3 {
                    if let triggerName = textFields[0].text, let name = textFields[1].text, let value = textFields[2].text, !triggerName.isEmpty, !name.isEmpty, !value.isEmpty {
                        Spinner.showSpinner()
                        Appgain.fireAutomator(triggerName, personalizationData: [name:value]) { [weak self] response, dic, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                            print("fireAutoMator is:", response, dic, error)
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter appID & apiKey")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func getUserId() {
        if let id = Appgain.getUserID() {
            showMessage(title: "Your Id", message: id)
            print("Your Id: \(id)")
        }
    }
    
    func showMessage(title: String?, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func createChannel(type: String) {
        let alertController = UIAlertController(title: "Enter Data", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            if type == NotificationType.email() {
                textField.placeholder = "Email"
            } else {
                textField.placeholder = "Phone Number"
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 1 {
                    if let data = textFields[0].text, !data.isEmpty {
                        Spinner.showSpinner()
                        Appgain.createNotificationChannel(type, withData: data) { [weak self] response, dict, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            print("createNotificationChannel:",response, dict, error)
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter trigger point")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func toggleNotification() {
        Spinner.showSpinner()
        Appgain.enableNotifications() { [weak self] response, dict, error in
            guard let self = self else { return }
            print("enableNotifications:", response, dict, error)
            Spinner.hideSpinner()
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                    showMessage(title: "Success", message: "Success")
                } else {
                    showMessage(title: "Error", message: response?.description ?? "")
                }
                print("dic is: \(httpResponse.statusCode)")
            }else {
                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    func logEvent() {
        let alertController = UIAlertController(title: "Enter Event Name", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Event Name"
        }
        alertController.addTextField { textField in
            textField.placeholder = "Action"
        }
        alertController.addTextField { textField in
            textField.placeholder = "key"
        }
        alertController.addTextField { textField in
            textField.placeholder = "value"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 1 {
                    if let eventName = textFields[0].text, !eventName.isEmpty,
                       let action = textFields[1].text, !action.isEmpty,
                       let key = textFields[2].text, !key.isEmpty,
                       let value = textFields[3].text, !value.isEmpty{
                        Spinner.showSpinner()
                        Appgain.logEvent(eventName, andAction: action, extras: [key:value]) {[weak self] response,dict, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            print("logEvent:",response, dict, error)
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter trigger point")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func logPurchase() {
        let alertController = UIAlertController(title: "Enter Data", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Purchase Name"
        }
        alertController.addTextField { textField in
            textField.placeholder = "Amount"
        }
        alertController.addTextField { textField in
            textField.placeholder = "Currency"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 3 {
                    if let purchaseName = textFields[0].text, let amount = textFields[1].text, let currency = textFields[2].text, !purchaseName.isEmpty, !amount.isEmpty, !currency.isEmpty {
                        Spinner.showSpinner()
                        Appgain.logPurchase(purchaseName, withAmount: Double(amount) ?? 0.0, forCurrency: currency) { [weak self] response, dic, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                            print("logPurchase is:", response, dic, error)
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter appID & apiKey")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func updateUserData() {
        let alertController = UIAlertController(title: "Enter Data", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "key"
        }
        alertController.addTextField { textField in
            textField.placeholder = "value"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            // Handle the OK button tap
            if let textFields = alertController.textFields {
                if textFields.count >= 1 {
                    if let key = textFields[0].text, !key.isEmpty,
                       let value = textFields[1].text, !value.isEmpty {
                        Spinner.showSpinner()
                        Appgain.updateUserData([key : value]) {[weak self] response,dict, error in
                            guard let self = self else { return }
                            Spinner.hideSpinner()
                            print("updateUserData:",response, dict, error)
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    showMessage(title: "Success", message: "Success")
                                } else {
                                    showMessage(title: "Error", message: response?.description ?? "")
                                }
                                print("dic is: \(httpResponse.statusCode)")
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter data")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func updateUserID() {
        let alertController = UIAlertController(title: "Enter User Id", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "User Id"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            if let textFields = alertController.textFields {
                if textFields.count >= 1 {
                    if let newUserId = textFields[0].text, !newUserId.isEmpty{
                        Spinner.showSpinner()
                        Appgain.updateUserId(newUserId) { response, dict, error in
                            Spinner.hideSpinner()
                            print("updateUserID:",response, dict, error)
                            if let httpResponse = response as? HTTPURLResponse {
                                if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 204 {
                                    self.showMessage(title: "New id", message: "\(Appgain.getUserID() ?? "")")
                                } else {
                                    self.showMessage(title: "Error", message: response?.description ?? "")
                                }
                            }else {
                                self.showMessage(title: "Error", message: error?.localizedDescription ?? "")
                            }
                        }
                    } else {
                        self.showMessage(title: "Error", message: "Please Enter Id")
                    }
                }
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func requestTracking() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { [weak self] status in
                guard let self = self else { return }
                switch status {
                case .authorized:
                    print("enable tracking")
                    showMessage(title: "Success", message: "enable tracking")
//                    AppGainManager.shared.updateUserId()
                case .denied:
                    print("disable tracking")
                    showMessage(title: "Failed", message: "disable tracking")
                case .notDetermined:
                    print("notDetermined tracking")
                    showMessage(title: "Failed", message: ("notDetermined tracking"))
                case .restricted:
                    print("restricted tracking")
                    showMessage(title: "Failed", message: "restricted tracking")
                default:
                    print("disable tracking")
                    showMessage(title: "Failed", message: "disable tracking")
                }
            }
        }
    }

    
    @IBAction func revealMenu(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
}

extension TestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        testCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TestActionsTableViewCell.className, for: indexPath) as! TestActionsTableViewCell
        cell.setData(title: testCases[indexPath.row].title, image: testCases[indexPath.row].image)
        cell.setCellStyle(backgoundColor: UIColor(hex: 0xF6F7FB, a: 0.8), style: .none)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemSelected = testCases[indexPath.row]
        switch itemSelected {
        case .initalize:
            print("initalize")
            initializeSDK()
        case .matchLink:
            print("matchLink")
            matchLink()
        case .fireAutoMator:
            print("fireAutoMator")
            fireAutoMator()
        case .fireAutoMatorWith:
            print("fireAutoMatorWith")
            fireAutomatorWithPersonalization()
        case .addPurchase:
            print("addPurchase")
            logPurchase()
        case .addEmailChannel:
            print("addEmailChannel")
            createChannel(type: NotificationType.email())
        case .addSmsCahnnel:
            print("addSmsCahnnel")
            createChannel(type: NotificationType.sms())
        case .logEvent:
            print("logEvent")
            logEvent()
        case .getUserID:
            print("getUserID")
            getUserId()
        case .updateUser:
            print("updateUser")
            updateUserData()
        case .tracking:
            print("tracking")
            requestTracking()
        case .updateUserID:
            print("updateUser")
            updateUserID()
        case .toggleNotification:
            print("toggleNotification")
            toggleNotification()
        }
    }
}

