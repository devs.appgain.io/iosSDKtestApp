//
//  TestActionsTableViewCell.swift
//  AppGainTest
//
//  Created by Monica Girges on 07/09/2023.
//

import UIKit

class TestActionsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var featureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        mainView.makeRoundedCornersWith(radius: 8.0)
    }
    
    func setCellStyle(backgoundColor: UIColor, style: UITableViewCell.SelectionStyle) {
        selectionStyle = style
        mainView.backgroundColor = backgoundColor
    }
    
    func setData(title: String, image: String) {
        titleLabel.text = title
        featureImageView.image = UIImage(named: image)
    }
}
