//
//  AppGainTestEnum.swift
//  AppGainTest
//
//  Created by Monica Girges on 07/09/2023.
//

import Foundation
import UIKit

enum AppGainTestEnum {
    case initalize
    case matchLink
    case fireAutoMator
    case tracking
    case fireAutoMatorWith
    case addPurchase
    case addEmailChannel
    case addSmsCahnnel
    case logEvent
    case toggleNotification
    case getUserID
    case updateUserID
    case updateUser
    
    var title: String {
        switch self {
        case .initalize:
            return "Initalize"
        case .matchLink:
            return "Match Link"
        case .fireAutoMator:
            return "Fire Automator"
        case .fireAutoMatorWith:
            return "Fire automator with personalization"
        case .addPurchase:
            return "Add Purchase"
        case .addEmailChannel:
            return "Add Email Channel"
        case .addSmsCahnnel:
            return "Add Sms Channel"
        case .logEvent:
            return "Log Event"
        case .getUserID:
            return "Get UserID"
        case .updateUser:
            return "Update User"
        case .tracking:
            return "Init Advertiser Id Tracking"
        case .updateUserID:
            return "Update UserID"
        case .toggleNotification:
            return "Toggle Notification"
        }
    }
    
    var image: String {
        switch self {
        case .initalize:
            return "init"
        case .matchLink:
            return "matchLink"
        case .fireAutoMator:
            return "fireAutomator"
        case .fireAutoMatorWith:
            return "fireAutomatorPerson"
        case .addPurchase:
            return "purchase"
        case .addEmailChannel:
            return "email"
        case .addSmsCahnnel:
            return "sms"
        case .logEvent:
            return "log"
        case .getUserID:
            return "user"
        case .updateUser:
            return "updateUser"
        case .tracking:
            return "initAdvertiser"
        case .updateUserID:
            return "updateUser"
        case .toggleNotification:
            return "log"
        }
    }
}
