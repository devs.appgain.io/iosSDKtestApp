//
//  OnBoardingViewController.swift
//  AppGainTest
//
//  Created by Monica Girges on 05/10/2023.
//

import UIKit

class OnBoardingViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var circularProgressView: CircularProgressView!
    @IBOutlet weak var secondaryImageView: UIImageView!
    private var data = ["onBoarding1", "onBoarding2", "onBoarding3"]
    private var timer: Timer?
    @IBOutlet weak var pageControll: UIPageControl!
    private var currentCellIndex = 0
    private var progressValue = 0.4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        startTimer()
    }
    
    func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        pageControll.numberOfPages = data.count
        circularProgressView.setprogress(progressValue, UIColor(hex: 0x024DA1), "", "")
        collectionView.register(UINib(nibName: OnBoardingCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OnBoardingCollectionViewCell.className)
    }
    
    func startTimer() {
        timer = Timer(timeInterval: 2.5, target: self, selector: #selector(moveToNextIndex), userInfo: nil, repeats: true)
    }

    @objc func moveToNextIndex() {
        if currentCellIndex < data.count - 1 {
            currentCellIndex += 1
            pageControll.currentPage = currentCellIndex
            secondaryImageView.isHidden = true
            collectionView.scrollToItem(at: IndexPath(item: currentCellIndex, section: 0), at: .centeredHorizontally, animated: true)
            progressValue = progressValue + 0.4
            circularProgressView.setprogress(progressValue, UIColor(hex: 0x024DA1), "", "")
        } else {
            currentCellIndex = 0
            let vc = BaseNavigationViewController(rootViewController: MySideMenuViewController())
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
        }
    }
    
    @IBAction func didPressNextButton(_ sender: UIButton) {
        moveToNextIndex()
    }
    
    @IBAction func didPressSkipButton(_ sender: UIButton) {
        let vc = BaseNavigationViewController(rootViewController: MySideMenuViewController())
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
}

extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnBoardingCollectionViewCell.className, for: indexPath) as! OnBoardingCollectionViewCell
        cell.onBoardingImageView.image = UIImage(named: data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControll.currentPage = indexPath.row
    }
    
}
