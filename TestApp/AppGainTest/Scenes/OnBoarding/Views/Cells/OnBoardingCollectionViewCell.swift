//
//  OnBoardingCollectionViewCell.swift
//  AppGainTest
//
//  Created by Monica Girges on 05/10/2023.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var onBoardingImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
