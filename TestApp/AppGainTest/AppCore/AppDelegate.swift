//
//  AppDelegate.swift
//  AppGainTest
//
//  Created by Monica Girges on 06/09/2023.
//

import UIKit
import CoreData
import Appgain
import Appgain_rich
import AppTrackingTransparency
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        registerForPushNotifications()
        return true
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            DispatchQueue.main.async {
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        }
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }

    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        Appgain.registerDevice(withToken: deviceToken)
    }
    
    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Payload UserInfo: \(userInfo)")
        completionHandler(.newData)
        Appgain.handlePush(userInfo, for: application)
    }
    
    //MARK: - FETCH NOTIFICATION DATA
    // Handle notifications when the app is in the foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("Payload UserInfo: \(userInfo)")
        completionHandler([.alert,.sound,.badge])
        _ = notification.request.content.categoryIdentifier
        Appgain.handlePush(userInfo, for: nil)
    }
    
    // Handle user interactions with notifications
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        _ = response.notification.request.content.categoryIdentifier
        let userInfo = response.notification.request.content.userInfo
        print("Payload UserInfo: \(userInfo)")
        completionHandler()
        Appgain.recordPushStatus(NotificationStatus.opened(), userInfo: userInfo)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didDismissNotification notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        print("Payload UserInfo: \(userInfo)")
        Appgain.recordPushStatus(NotificationStatus.dismissed(), userInfo: userInfo)
    }
    
//    // Process the notification and display it in the appropriate language
//    private func processNotification(userInfo: [AnyHashable: Any], completionHandler: @escaping (Bool) -> Void) {
//        let deviceLanguage = Locale.preferredLanguages.first?.split(separator: "-").first ?? "en"
//        
//        guard let aps = userInfo["aps"] as? [String: Any],
//              deviceLanguage != "en",
//              let localizedTitle = aps["title-\(deviceLanguage)"] as? String,
//              let localizedBody = aps["alert-\(deviceLanguage)"] as? String else {
//            completionHandler(true)
//            return
//        }
//        
//        let content = UNMutableNotificationContent()
//        content.body = localizedBody
//        content.title = localizedTitle
//        
//        let request = UNNotificationRequest(identifier: "notificationIdentifier", content: content, trigger: nil)
//        UNUserNotificationCenter.current().add(request) { error in
//            if let error = error {
//                print("Error delivering notification: \(error)")
//                completionHandler(true)
//            } else {
//                completionHandler(false)
//            }
//        }
//    }
}
