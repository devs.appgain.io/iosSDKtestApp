//
//  SceneDelegate.swift
//  AppGainTest
//
//  Created by Monica Girges on 06/09/2023.
//

import UIKit
import Appgain
import Appgain_rich
import AppTrackingTransparency

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var openedURL: URL?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        
        print(UserDefaults.standard.bool(forKey: "notFirstLaunch"))
        if UserDefaults.standard.bool(forKey: "notFirstLaunch") {
            window.rootViewController = BaseNavigationViewController(rootViewController: MySideMenuViewController())
            window.makeKeyAndVisible()
        } else {
            UserDefaults.standard.set(true, forKey: "notFirstLaunch")
            window.rootViewController = OnBoardingViewController()
            window.makeKeyAndVisible()
        }
        
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        
        // Save changes in the application's managed object context when the application transitions to the background.
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        if let url = userActivity.webpageURL, openedURL != url  {
            openedURL = url
            print("url:", url)
            if url.scheme == "TestApp" {
                if url.host == "details_screen" {
                    if let movieId = url.pathComponents.last {
                        if let dynamicLinkURL = DynamicLinkGenerator.generateDynamicLink() {
                            UIApplication.shared.open(dynamicLinkURL, options: [:], completionHandler: nil)

                        }
                    }
                }
            }
        }
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = URLContexts.first?.url, openedURL != url {
            openedURL = url
            print("url:", url)
            if url.scheme == "TestApp" {
                if url.host == "details_screen" {
                    if let movieId = url.pathComponents.last {
                        if let dynamicLinkURL = DynamicLinkGenerator.generateDynamicLink() {
                            UIApplication.shared.open(dynamicLinkURL, options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        }
    }
}

