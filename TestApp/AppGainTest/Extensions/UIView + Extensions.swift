//
//  UIView + Extensions.swift
//  FFLASHH
//
//  Created by Moca on 5/26/21.
//

import UIKit


extension UIView {
    
    func makeRoundedCorners(){
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.height / 2
    }
    
    func makeRoundedCornersUsingWidth() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width / 2
    }
    
    func makeRoundedCornersWith(radius: CGFloat){
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
    
    func makeShadows(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.masksToBounds = false
    }
    
    func dropShadow(radius: CGFloat = 1, scale: Bool = true) {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.masksToBounds = false
        layer.shadowRadius = radius

        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = 1
      }
    
    func makeShadowsWith(color: CGColor, opacity: Float, shadowRadius: CGFloat){
        self.layer.shadowColor = color
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
    }
    
    func removeShadow() {
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 0.0
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.0
    }
    
    func makeBorders(borderColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)){
        self.layer.borderWidth = 1
        self.layer.borderColor = borderColor.cgColor
    }
    
    func removeBorders() {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func setBorderColor(color: UIColor,width: CGFloat){
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
           let offset: CGFloat = self.frame.width / desiredCurve!
           let bounds: CGRect = self.bounds
           
           let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
           let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
           let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
           let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
           rectPath.append(ovalPath)
           
           // Create the shape layer and set its path
           let maskLayer: CAShapeLayer = CAShapeLayer()
           maskLayer.frame = bounds
           maskLayer.path = rectPath.cgPath
           
           // Set the newly created shape layer as the mask for the view's layer
           self.layer.mask = maskLayer
       }
}
