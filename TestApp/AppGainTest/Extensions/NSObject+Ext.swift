//
//  NSObject+Ext.swift
//  AppGainTest
//
//  Created by Monica Girges on 07/09/2023.
//

import Foundation

extension NSObject {
    
    public var className: String {
        return type(of: self).className
    }

    public static var className: String {
        return String(describing: self)
    }
    
}
