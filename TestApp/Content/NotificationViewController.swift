//
//  NotificationViewController.swift
//  Content
//
//  Created by Monica Girgis Kamel on 13/09/2023.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import Appgain_rich

class NotificationViewController: UIViewController, UNNotificationContentExtension {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        AppgainRich.didReceive(notification, in: self)
    }

}
